#!/usr/bin/python3

import sys
import requests
import datetime
import argparse


def payloads_from_file(fp, payload_fmt):
    """ Extract payloads
        Arguments :
            - fp : opened file containing payloads
            - payload_fmt : payload printf format
        Returns :
            Generator on payloads
    """
    for line in fp:
        payload = line.strip()
        if payload.startswith("**"):
            print("Trying : " + payload)
        elif payload:
            yield payload_fmt % payload


def test_payload(payload, target, magic_word="Hacker"):
    """ Send payload and format result
        Arguments :
            payload : payload str
            target : target URL
            payload_format
        Returns : True if payload is authorized False if filtered
    """
    data_dict = {"title": "title", "message": payload, "submit": "Envoyer"}
    headers = {"Content-type": "application/x-www-form-urlencoded", "Accept": "text/html"}
    try:
        response = requests.post(target, headers=headers, data=data_dict)
        print(payload)
        return magic_word not in response.text
    except requests.exceptions.RequestException as e:
        print("[-] ConnectionError : Send your credential to root-me, or connect via web app before use this script ")
        raise SystemExit(e)


if __name__ == '__main__':
    PAYLOAD_FORMAT = "%s"
    file_tok = datetime.datetime.now().isoformat(timespec='seconds')
    parser = argparse.ArgumentParser()
    parser.add_argument('input', type=argparse.FileType('r'),
                        default=sys.stdin,
                        nargs="?",
                        help="Input filename ('-' for stdin, default)")
    parser.add_argument('output', type=str,
                        default='authorize-%s.txt' % file_tok,
                        nargs="?",
                        help="Input filename ('-' for stdout, default is authorize-DATE.txt)")
    parser.add_argument('-t', '--target-url', type=str,
                        default='http://challenge01.root-me.org/web-client/ch21/',
                        help="target_url")

    args = parser.parse_args()
    out_fp = open(args.output, 'w+') if args.output != '-' else sys.stdout

    payloads = payloads_from_file(args.input, PAYLOAD_FORMAT)
    for payload in payloads:
        if test_payload(payload, args.target_url):
            out_fp.write("%s\n" % payload)

    out_fp.close()
    args.input.close()
    exit(0)
