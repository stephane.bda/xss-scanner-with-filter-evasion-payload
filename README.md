# XSS Scanner with Filter Evasion payload

What is he doing :

1- Automatic fetch filter evasion payloads from [OWASP XSS Filter Evasion Cheat Sheet](https://owasp.org/www-community/xss-filter-evasion-cheatsheet)

    - Fetch & save different kind of filter evasion & associated payloads

2- Automatic Submit payloads

    - Display kind of filter evasion with : Trying : **
    
    - Display & Submit payloads

3- Automatic read server response & Save the non filtered payloads in "authorize-DATE.txt" format

Initially built for XSS filter evasion challenge


![](xss_scanner.gif)


## Require:
- Python 3

### Python Module :

- BeautifulSoup 4

Debian install : 
```bash
sudo apt-get install python3-bs4
```
Or PIP install :
```bash
pip3 install beautifulsoup4
```

- Requests

Debian install : 
```bash
sudo apt-get install python3-requests
```
Or PIP install :
```bash
pip3 install requests
```

## Run :

## if you want to fetch new payload from OWASP and scan with them
Make the xss_scanner script executable:
```bash
chmod +x xss_scanner.py
```

### Then you can pipe the extract_code and the XSS scanner
```bash
python3 extract_code.py | ./xss_scanner.py
```

## if you want to only extract & save payload list from OWASP
Run the extractor:
```bash
python3 extract_code.py result.txt
```

# Multiple Usage :
```bash
python3 extract_code.py | ./xss_scanner.py
```
```bash
./xss_scanner.py your_payloads.txt
```
```bash
cat payloads1.txt payloads2.txt | ./xss_scanner.py
```
```bash
./xss_scanner.py payloads.txt result.txt
```
```bash
cat payloads1.txt payloads2.txt | ./xss_scanner.py - result.txt
```
```bash
cat xss_payload_all/xss* | ./xss_scanner.py
```
```bash
./xss_scanner.py payloads.txt -
```

