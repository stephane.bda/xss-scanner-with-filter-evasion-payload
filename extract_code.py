#!/usr/bin/python3

import sys
import argparse
import bs4
import requests


def extract_code_from(link):
    """Extract code (payload) & tag content from url."""
    result = list()
    response = requests.get(link, timeout=10)
    soup = bs4.BeautifulSoup(response.text, "html.parser")

    for h2 in soup.find_all('h2'):
        try:
            if h2 == "<h2>Corporate Supporters</h2>":  # End page
                pass
            code = h2.find_next('code')
            sep = "** "
            h2_and_sep = sep + h2.text
            result.append(h2_and_sep)
            result.append(code.text)
        except AttributeError:
            pass

    return result


def save_result(result, fp):
    """Save Result in txt file, see extract_code_from."""
    fname = 'xss_payload_owasp_filter_evasion.txt'
    for element in result:
        if element:
            fp.write(element)
            fp.write('\n')


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Fetch payload from owasp")
    parser.add_argument('output',
                        type=argparse.FileType('w'),
                        default=sys.stdout,
                        nargs='?')
    parser.add_argument('-u', '--url',
                        type=str,
                        default="https://owasp.org/www-community/xss-filter-evasion-cheatsheet")
    args = parser.parse_args()

    result = extract_code_from(args.url)
    save_result(result, args.output)
    args.output.close()
